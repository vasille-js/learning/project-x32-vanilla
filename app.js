
let map = document.getElementById("map");
let isAnim = false;
let rtx = 0, rty = 0, rscale = 1;
let btx, bty, bscale;

function step (scale) {
    if (scale <= 1) {
        return 1;
    }
    if (scale <= 2) {
        return 2;
    }
    if (scale <= 4) {
        return 4;
    }
    if (scale <= 8) {
        return 8;
    }
    if (scale <= 16) {
        return 16;
    }
    if (scale <= 32) {
        return 32;
    }
    return scale;
}

function fixXClass (scale) {
    let s = step(scale);

    if (map.className !== 'x' + s) {
        map.className = 'x' + s;
    }
}

function animTo (x, y, scale) {
    rtx = x;
    rty = y;
    rscale = scale;
    btx = parseFloat(map.style.getPropertyValue("--tx"));
    bty = parseFloat(map.style.getPropertyValue("--ty"));
    bscale = parseFloat(map.style.getPropertyValue("--scale"));

    let beginTime = new Date().getTime();
    let endTime = beginTime + 300;

    if (!isAnim) {
        isAnim = true;

        let update = () => {
            let now = new Date().getTime();

            if (now > endTime) {
                isAnim = false;
                map.style.setProperty("--tx", rtx + 'px');
                map.style.setProperty("--ty", rty + 'px');
                map.style.setProperty("--scale", rscale);
                fixXClass(rscale);
            }
            else {
                let progress = (now - beginTime) / 300;
                let step = Math.cos(Math.PI * progress) - 1;
                let nscale = -(rscale - bscale) / 2 * step + bscale;

                map.style.setProperty("--tx", -(rtx - btx) / 2 * step + btx + 'px');
                map.style.setProperty("--ty", -(rty - bty) / 2 * step + bty + 'px');
                map.style.setProperty("--scale", nscale);
                fixXClass(nscale);

                window.requestAnimationFrame(update);
            }
        };

        window.requestAnimationFrame(update);
    }
}

document.onmousedown = press => {
    let tx = parseFloat(map.style.getPropertyValue("--tx"));
    let ty = parseFloat(map.style.getPropertyValue("--ty"));

    press.preventDefault();

    document.onmousemove = move => {
        rtx = tx + (move.screenX - press.screenX);
        rty = ty + (move.screenY - press.screenY);
        map.style.setProperty("--tx", rtx + 'px');
        map.style.setProperty("--ty", rty + 'px');

        move.preventDefault();
    }

    document.onmouseup = up => {
        document.onmousemove = null;
        document.onmouseup = null;
        up.preventDefault();
    }
}

document.onwheel = wheel => {
    let cscale = rscale || 1;
    let ctx = rtx || 0;
    let cty = rty || 0;
    let step1 = step(cscale);
    let nscale;

    if (wheel.deltaY < 0) {
        let v = cscale + 0.1 * step1;

        if (v < 32) {
            nscale = v;
        }
        else {
            nscale = 32;
        }
    }
    else {
        let v = cscale - 0.1 * step1;

        if (v > 0.5) {
            nscale = v;
        }
        else {
            nscale = 0.5;
        }
    }

    animTo(
        ctx - (nscale - cscale) * (wheel.clientX - ctx) / cscale,
        cty - (nscale - cscale) * (wheel.clientY - cty) / cscale,
        nscale
    );
}

window.onkeypress = () => {
    document.onwheel({deltaY: -1, clientX: 0, clientY: 0});
}
